﻿using System;
using System.IO;
using System.Linq;
using FileMonitoring.Interfaces;

namespace FileMonitoring
{
	public class FileMonitor : IFileMonitor
	{
		private FileSystemWatcher _watcher;
		private readonly IConfiguration _configuration;

		public FileMonitor(IConfiguration configuration)
		{
			_configuration = configuration;
			Directory.CreateDirectory(_configuration.BackupPath);
		}

		public void Start()
		{
			foreach (var file in Directory.GetFiles(_configuration.Path, "*.txt"))
			{
				SaveCopy(file);
			}

			_watcher = new FileSystemWatcher(_configuration.Path, "*.txt") {EnableRaisingEvents = true};
			_watcher.Changed += WatcherOnChanged;
		}

		private void WatcherOnChanged(object sender, FileSystemEventArgs e)
		{
			if (e.ChangeType == WatcherChangeTypes.Changed)
			{
				SaveCopy(e.FullPath);
			}
		}

		public void Stop()
		{
			_watcher.Changed -= WatcherOnChanged;
			Directory.Delete(_configuration.BackupPath, true);
		}

		public void Reset(DateTime dateTime)
		{
			var backups = Directory.GetDirectories(_configuration.BackupPath).SelectMany(Directory.GetFiles).Select(f => new FileInfo(f)).ToList();
			var groupedBackups = backups.GroupBy(f => f.Directory?.Name);
			var originalFiles = Directory.GetFiles(_configuration.Path);
			foreach (var fileGroup in groupedBackups)
			{
				var fileToRestore = fileGroup
					.Where(f => f.CreationTime <= dateTime)
					.OrderByDescending(f => f.CreationTime)
					.FirstOrDefault();

				if (fileToRestore == null)
					continue;

				var originFile = originalFiles.FirstOrDefault(f => Path.GetFileNameWithoutExtension(f) == fileToRestore.Directory?.Name);
				if (originFile != null)
				{
					try
					{
						_watcher.Changed -= WatcherOnChanged;
						File.Copy(fileToRestore.FullName, originFile, true);
					}
					finally
					{
						_watcher.Changed += WatcherOnChanged;
					}
				}
			}
		}

		public void Dispose()
		{
			Stop();
		}

		private void SaveCopy(string path)
		{
			using (var reader = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read))
			{
				var backupDirectory = Path.Combine(_configuration.BackupPath, Path.GetFileNameWithoutExtension(path));
				Directory.CreateDirectory(backupDirectory);
				var backupFileName = Path.Combine(backupDirectory, Guid.NewGuid() + ".txt");
				using (var writer = File.Open(backupFileName, FileMode.CreateNew, FileAccess.Write, FileShare.None))
				{
					reader.CopyTo(writer);
					writer.Flush();
				}
			}
		}
	}
}
